import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

/**
 *
 * @author ramanzes
 * возвращает имя файла либо с полным путём к нему либо просто имя.
 */
public class FWorks {

    public static String getFilename(String file, Boolean fullpath) throws FileNotFoundException {

        String path = "";
        String fullname = "";

        path = "files/";
        //из этого файла считываем имя первого файла. т.е. newfile1.txt
        fullname = path + file;

//        InputStream stream = new FileInputStream(fullname);

//        Scanner filescan = new Scanner(stream);

        if (!fullpath) {
            return file;
//            return (filescan.next());
        } else {
            return (fullname);
//            return (path + filescan.next());
        }
    }

    public static String getStringFromFile(String file, int numStr) throws FileNotFoundException {

        String path = new String();
        StringBuilder sb = new StringBuilder();
        String fullname = new String();
        String result ="";

        path = "files/";
        //из этого файла считываем имя первого файла. т.е. files/1
//        file="1";
        fullname=path+file;

        InputStream stream = new FileInputStream(fullname);

        Scanner filescan = new Scanner(stream);

        for (int i=1; i<=numStr;i++) {
            result=(filescan.nextLine());
        }
        return result;
    }



}
