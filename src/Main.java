import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 *
 * @author ramanzes
 */
public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("Hello World!");




//    Задание
//    :
//1.
//Выведите числа от 0 до миллиона

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!");

    var i = 0;
    while (i<=1000000){
        System.out.print(" ("+i+"); ");
        i+=1;
    }



//    2.
//Выведите числа от 1.5 до 101.5: 1.5,2,2.5,3,3.5...101.5


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        var i2 = 0;

        while (i2<=100){
            System.out.print(" ("+(1.5+i2)+"); ");
            i2+=1;
        }

//3.
//Выведите латинский алфавит от a до z
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        int i3='a';
        while (i3<='z'){
            System.out.println((char)i3);
            i3+=1;
        }

//    4.
//Выведите русский алфавит от а до я
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        int i4='а';
        while (i4<='я'){
            System.out.println((char)i4);
            i4+=1;
        }



//    5.
//Создайте 10 тысяч файлов
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        int i5=1;
        while (i5<=10_000){
//        File file = new File("/home/ramanzes/myapp/synergy/2_1/NetBeans/tema2_urok1/mavenproject1/src/main/java/com/mycompany/mavenproject1/files/"+i5);
            File file = new File(FWorks.getFilename(String.valueOf(i5), Boolean.TRUE));

            System.out.println("создан файл files/"+i5 + file.createNewFile());

            i5+=1;

        }






//    6.
//В файле две строки
//    : секретное слово и подсказка
//    . Вывести подсказку
//    .
//Считывать строку за строкой, пока игр ок не отгадает
//
//    секретное слово(не введёт
//    его)

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println(FWorks.getStringFromFile(FWorks.getFilename("file",false),1));
        System.out.println("Введите это слово");
        Scanner scanner = new Scanner(System.in);
        while (!scanner.nextLine().equals(FWorks.getStringFromFile(FWorks.getFilename("file",false),2))){
            System.out.println(FWorks.getStringFromFile(FWorks.getFilename("file",false),1));
            System.out.println("Введите это слово");
        }
        System.out.printf("Вы верно ввели это слово");
//7.
//Предыдущее задание, но если пользователь ввел хотя бы часть слова верно,
//то писать: горячо. (Проверять с помощью str.contains).
    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println(FWorks.getStringFromFile(FWorks.getFilename("file",false),1));
        System.out.println("Введите это слово");
        String str6 = "";
        str6=scanner.nextLine();
        String str6_sec=FWorks.getStringFromFile(FWorks.getFilename("file",false),2);
        if (str6_sec.contains(str6)){
            System.out.println("горячо");
        } else System.out.println("холодно");

        while (!str6.equals(str6_sec)){
            System.out.println(FWorks.getStringFromFile(FWorks.getFilename("file",false),1));
            System.out.println("Введите это слово");
            str6=scanner.nextLine();
            if (str6_sec.contains(str6)){
                System.out.println("горячо");
            } else System.out.println("холодно");
        }
        System.out.println("Вы верно ввели это слово");
//8.
//Первый игрок сохраняет слово и подсказку в первый файл, второй игрок во
//второй. И
//гра начинается, выводятся подсказки, игроки по
//-
//очереди
//пытаются отгадать слово противника.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        String file1="files/file1";
        String file2="files/file2";
        String buff1="",buff2="";
        //создадим писателей в файлы
        FileWriter writer1=new FileWriter(file1);
        FileWriter writer2=new FileWriter(file2);

        //отчистим содержимое файлов если они были созданы ранее другими игроками
        PrintWriter writerClear1 = new PrintWriter(file1);
        writerClear1.print("");
        writerClear1.close();

        PrintWriter writerClear2 = new PrintWriter(file2);
        writerClear2.print("");
        writerClear2.close();

        System.out.println("*********************************************************************************");
        System.out.println("Первый игрок вводит свою подсказку:");
        writer1.write(scanner.nextLine());
        writer1.write("\n");
        System.out.println("Первый игрок вводит свой ответ:");
        writer1.write(scanner.nextLine());
        writer1.close();
        System.out.println("*********************************************************************************");
        System.out.println("Второй игрок вводит свою подсказку:");
        writer2.write(scanner.nextLine());
        writer2.write("\n");
        System.out.println("Второй игрок вводит свой ответ:");
        writer2.write(scanner.nextLine());
        writer2.close();
        System.out.println("*********************************************************************************");

        String hint1="",result1="", hint2="", result2="";
        //считываем значения из файлов
        hint1=FWorks.getStringFromFile("file1",1);
        hint2=FWorks.getStringFromFile("file2",1);
        result1=FWorks.getStringFromFile("file1",2);
        result2=FWorks.getStringFromFile("file2",2);
        //Игра начинается. цикл продолжается до тех пор пока кто либо из них не угадал слово по подсказке

        do {
            System.out.println("*********************************************************************************");
            System.out.println("игрок 1 введите секретное слово игрока 2 по следующей подсказке:");
            System.out.println(hint2);
            buff1 = scanner.nextLine();
            if (result2.contains(buff1)) {
                System.out.println("горячо");
            } else System.out.println("холодно");

            System.out.println("*********************************************************************************");
            System.out.println("игрок 2 введите секретное слово игрока 1 по следующей подсказке:");
            System.out.println(hint1);

            buff2 = scanner.nextLine();
            if (result1.contains(buff2)) {
                System.out.println("горячо");
            } else System.out.println("холодно");
            System.out.println("*********************************************************************************");
        }while (!result2.equals(buff1) && !result1.equals(buff2));

        if (result2.equals(buff1) && result1.equals(buff2)){
            System.out.println("ничья");
        } else if (result2.equals(buff1))
        {
            System.out.println("выиграл игрок 1");
        } else
            System.out.println("выиграл игрок 2");




//9.
//Пока пользователь не введёт строку, содержащую пробел, считывайте
//строки и выводите их первые буквы


  System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 9 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        String buff9="";
        do {
      System.out.println("Введите строку, пока в строке не будет пробела это будет продолжаться:");
      buff9 = scanner.nextLine();
      System.out.println("Первый символ введённой строки это <"+buff9.charAt(0)+">");
  } while (!buff9.contains(" "));
        System.out.println(" в строке был пробел поэтому 9-е задание закончилось");
  //10.
//Пользователь вводит полный путь и название файла. Пока пользоват
//ель не
//ввел путь к существующему файлу, повторять ввод. Проверить, что файл
//существует, можно так: File f = new File(); boolean isExists = f.exists();

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 10 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        String buff10="";
        boolean ok=false;
        do {

            System.out.println("Введите путь и имя существующего файла например:  files/file :");
            buff10 = scanner.nextLine();
            File f = new File(buff10);
            if (f.exists()) {

                if (f.isFile()) {
                    System.out.println("Такой файл существует");
                    ok = true;
                } else System.out.println("Вы близки к цели, но это каталог, а не файл");
            }
            else {
                System.out.println("такого файла нет");
            }
        } while (!ok);
        System.out.println(" файл найден 10-е задание закончилось");
//    11.
//Сохраните снимки NASA за январь 2022 года
 String myIpKey="b8BKYPTRSYvG6dKnwUbPAq8jht3kMxyrJ6WVBVRp";

        //у меня не стал работать этот api поэтому взял другой аналогичный
        //https://breakingbadapi.com/api/quote/random
        byte day=1;
// сразу все ссылки через get параметры api так быстрее, но задание про циклы
//            String page = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey + "&start_date=" + "2022-01-01" + "&end_date=" + "2022-01-31");

// по одной ссылке через цикл, раз задания про циклы, но так дольше...
        String dd="";
        while (day!=31) {
            if (day<10) dd="0"+day;
            else dd=""+day;
            String page = downloadWebPage("https://api.nasa.gov/planetary/apod?api_key=" + myIpKey + "&start_date=" + "2022-01-"+dd + "&end_date=" + "2022-01-"+dd);
            System.out.println(page);
            day+=1;
        }


    }
    private static String downloadWebPage(String url) throws IOException{
        StringBuilder result = new StringBuilder();
        String line;
        URLConnection urlConnection = new URL(url).openConnection();
        try (InputStream is = urlConnection.getInputStream();
             BufferedReader br= new BufferedReader(new InputStreamReader(is))){
            while ((line=br.readLine())!=null){
                result.append(line);
            }
        }
        return result.toString();



}
}